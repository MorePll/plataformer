﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Camera : MonoBehaviour
{
    public List<GameObject> players;
    private Camera _camera;
    public float dampTime = 0.15f;
    public float smoothTime = 2f;
    public float zoomvalue;
    private Vector3 velocity = Vector3.zero;
    private float posCamY;


    void Start()
    {
        _camera = GetComponent<Camera>();
        posCamY = 2f;
    }

    void LateUpdate()
    {
        if (GameManager.actualPlayer == 5)
        {
            posCamY = -3f;
        }
        else
        {
            posCamY = 3f;
        }
        if (players[GameManager.actualPlayer] != null) //si es judador no es nulo
        {
            Vector3 point = _camera.WorldToViewportPoint(players[GameManager.actualPlayer].transform.position); 
            //posicion del jugador desde las coordenadas de mundo a las cordenadas de vista de la camara

            Vector3 delta = players[GameManager.actualPlayer].transform.position - _camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
            //calcula diferencia entre pos del juegador y la pos de la camara para estar centrada

            // Ajusta la posición en Y de la cámara directamente en la componente Y de destination
            //Vector3 destination = transform.position + delta;
            Vector3 destination = new Vector3(transform.position.x + delta.x,players[GameManager.actualPlayer].transform.position.y + posCamY,transform.position.z + delta.z);
            //calcula la pos de destina de la cam, lambia el codigo para poder midificar levemente la posiicon de y

            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
            //mueve la camara. usa SmoothDamp para que se vea el movimiento, velocity de actualiza para la prima llamada
        }
    }
}
