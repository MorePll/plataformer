﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    public float jumpForce = 10; // Fuerza del salto del jugador
    public float speed = 5; // Velocidad de movimiento del jugador
    public int playerNumber; // Número del jugador, usado para identificar al jugador actual
    public Rigidbody rb; // Componente Rigidbody del jugador
    protected BoxCollider col; // Componente BoxCollider del jugador
    public LayerMask floor; // Máscara de capa para identificar el suelo
    internal RaycastHit downHit; // Información sobre las colisiones de los rayos lanzados
    public float distanceRay, downDistanceRay; // Distancias para los rayos
    private bool canJump;
    internal bool onFloor;
    public float climbForce = 5; // Fuerza de subida por la escalera

    private bool onLadder; // Indica si el jugador está en la escalera

    internal Quaternion rotation90 = Quaternion.Euler(0, 90, 0);
    internal Quaternion rotation180 = Quaternion.Euler(0, 180, 0); //valores de rotacion
    internal Quaternion rotation270 = Quaternion.Euler(0, 270, 0);

    public virtual void Start()
    {
        rb = GetComponent<Rigidbody>(); // Obtiene el componente Rigidbody
        col = GetComponent<BoxCollider>(); // Obtiene el componente BoxCollider
        // Restringe el movimiento en los ejes X y Z, y la rotación en todos los ejes
        rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
        onLadder = false;
    }

    public virtual void FixedUpdate()
    {
        if (GameManager.actualPlayer == playerNumber) // Comprueba si este jugador es el jugador actual
        {
            if (onLadder)
            {
                ClimbLadder(); // Llama al método de subir por la escalera
            }

                Movement(); // Llama al método de movimiento
        }
    }

    public virtual void Update()
    {
        if (GameManager.actualPlayer == playerNumber) // Comprueba si este jugador es el jugador actual
        {
            if (onLadder == false)
            {
                Jump(); // Llama al método de salto
            }
            if (IsOnSomething() && onLadder == false) // Comprueba si está sobre algo
            {
                canJump = true; // Puede saltar
            }
            else
            {
                canJump = false; // No puede saltar
            }
        }
        else
        {
            if (onFloor) // Comprueba si el jugador está en el suelo
            {
                // Congela la posición y la rotación del jugador
                rb.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;
            }
            else
            {
                if (IsOnSomething()) // Comprueba si está sobre algo
                {
                    if (downHit.collider.gameObject.CompareTag("Player")) // Comprueba si está sobre otro jugador
                    {
                        // Restringe el movimiento en el eje Z y la rotación
                        rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
                    }
                }
            }
        }
    }

    public virtual bool IsOnSomething()
    {
        // Comprueba si hay algo directamente debajo del jugador usando un BoxCast
        return Physics.BoxCast(transform.position, new Vector3(transform.localScale.x * 0.9f, transform.localScale.y / 3, transform.localScale.z * 0.9f), Vector3.down, out downHit, Quaternion.identity, downDistanceRay);
    }

    public virtual void Movement()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.rotation = rotation270;
            rb.velocity = new Vector3(-speed, rb.velocity.y, 0); // Mueve el jugador a la izquierda
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.rotation = rotation90;
            rb.velocity = new Vector3(speed, rb.velocity.y, 0); // Mueve el jugador a la derecha
        }
        else
        {
            transform.rotation = rotation180;
            rb.velocity = new Vector3(0, rb.velocity.y, 0); // Detiene el movimiento horizontal
        }
    }

    public virtual void Jump()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            if (canJump)
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse); // Aplica una fuerza de impulso hacia arriba para saltar
            }
        }
    }

    public virtual void ClimbLadder()
    {
        rb.useGravity = false; // Desactiva la gravedad
        if (Input.GetKey(KeyCode.W))
        {
            rb.velocity = new Vector3(rb.velocity.x, climbForce, 0); // Sube la escalera
        }
        else if (Input.GetKey(KeyCode.S))
        {
            rb.velocity = new Vector3(rb.velocity.x, -climbForce, 0); // Baja la escalera
        }
        else
        {
            rb.velocity = new Vector3(rb.velocity.x, 0, 0); // Detiene el movimiento vertical
        }

        // Movimiento lateral mientras está en la escalera
        if (Input.GetKey(KeyCode.A))
        {
            rb.velocity = new Vector3(-speed, rb.velocity.y, 0); // Mueve el jugador a la izquierda
        }
        else if (Input.GetKey(KeyCode.D))
        {
            rb.velocity = new Vector3(speed, rb.velocity.y, 0); // Mueve el jugador a la derecha
        }
    }

    public virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Water") || collision.gameObject.CompareTag("Lava")) // Comprueba si colisionó con el agua o lava
        {
            Destroy(this.gameObject);
            GameManager.gameOver = true; // Establece el estado del juego como terminado
        }
        if (collision.gameObject.CompareTag("Floor")) // Comprueba si colisionó con el suelo
        {
            onFloor = true; // Indica que el jugador está en el suelo
        }
    }

    public virtual void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor")) // Comprueba si dejó de colisionar con el suelo
        {
            onFloor = false; // Indica que el jugador ya no está en el suelo
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ladder"))
        {
            onLadder = true; // Indica que el jugador está en la escalera
            rb.useGravity = false; // Desactiva la gravedad
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ladder"))
        {
            onLadder = false; // Indica que el jugador salió de la escalera
            rb.useGravity = true; // Reactiva la gravedad
        }
    }
}
