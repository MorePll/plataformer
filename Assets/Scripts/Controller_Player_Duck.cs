using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_Duck : Controller_Player
{
    private Vector3 originalScale; 
    private bool isDucking;

    public override void Start()
    {
        base.Start(); // Llama al m�todo Start del padre
        originalScale = transform.localScale;
        isDucking = false;
    }

    public override void Update()
    {
        base.Update(); // Llama al m�todo Update del padre

       
        if (Input.GetKeyDown(KeyCode.S) && onFloor)
        {
            Duck();
        }
    
        else if (Input.GetKeyUp(KeyCode.S))
        {
            StandUp();
        }
    }


    private void Duck()
    {
        if (!isDucking) 
        {
            transform.localScale = new Vector3(originalScale.x, originalScale.y / 2, originalScale.z); // Reduce a la mitad la altura del objeto
            isDucking = true; 
        }
    }


    private void StandUp()
    {
        transform.localScale = originalScale; // Restaura la altura original del objeto
        isDucking = false;
    }
}
