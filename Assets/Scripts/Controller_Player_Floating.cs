﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_Floating : Controller_Player
{
    public override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Lava")) // Comprueba si colisionó con el agua
        {
            Destroy(this.gameObject); // Destruye el jugador
            GameManager.gameOver = true; // Establece el estado del juego como terminado
        }
        if (collision.gameObject.CompareTag("Floor"))
        {
            onFloor = true;
        }
        //This makes the player invulnerable to water.

        //Sobreescribe la funcion de Controller_Player ya que la funcion tiene el metodo override, haciendo que el codigo del sript Controller_Player dentro de la funcion OnCollisionEnter quede inutilisable
    }
}
