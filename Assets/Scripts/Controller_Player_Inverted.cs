﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_Inverted : Controller_Player
{

    protected Quaternion InvRotation90 = Quaternion.Euler(0, 90, 180);
    protected Quaternion InvRotation180 = Quaternion.Euler(0, 180, 180); //valores de rotacion
    protected Quaternion InvRotation270 = Quaternion.Euler(0, 270, 180); 

    public override void FixedUpdate()
    {
        base.rb.AddForce(new Vector3(0, 30f, 0));
        base.FixedUpdate();
    }

    public override bool IsOnSomething()
    {
        return Physics.BoxCast(transform.position, new Vector3(transform.localScale.x * 0.9f, transform.localScale.y / 3, transform.localScale.z * 0.9f), Vector3.up, out downHit, Quaternion.identity, downDistanceRay);
    }

    public override void Jump()
    {
        if (IsOnSomething())
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    public override void Movement()
    {
        if (Input.GetKey(KeyCode.A))// && canMoveLeft) // Comprueba si se está presionando la tecla A y puede moverse a la izquierda
        {
            transform.rotation = InvRotation270;
            rb.velocity = new Vector3(1 * -speed, rb.velocity.y, 0); // Mueve el jugador a la izquierda
        }
        else if (Input.GetKey(KeyCode.D))// && canMoveRight) // Comprueba si se está presionando la tecla D y puede moverse a la derecha
        {
            transform.rotation = InvRotation90;
            rb.velocity = new Vector3(1 * speed, rb.velocity.y, 0); // Mueve el jugador a la derecha
        }
        else
        {
            transform.rotation = InvRotation180;
            rb.velocity = new Vector3(0, rb.velocity.y, 0); // Detiene el movimiento horizontal
        }

        // Si no puede moverse a la izquierda, detiene el movimiento horizontal
        // if (!canMoveLeft)
        //     rb.velocity = new Vector3(0, rb.velocity.y, 0);

        // Si no puede moverse a la derecha, detiene el movimiento horizontal
        // if (!canMoveRight)
        //     rb.velocity = new Vector3(0, rb.velocity.y, 0);
    }
}
