using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    public ButtonController buttonController;
    private Renderer doorRenderer;
    private Collider doorCollider;
    public int doorID;

    private void Start()
    {
        doorRenderer = GetComponent<Renderer>();
        doorCollider = GetComponent<Collider>();
    }

    private void Update()
    {
        if (buttonController.IsPressed() && buttonController.buttonID == doorID)
        {
            OpenDoor();
        }
        else
        {
            CloseDoor();
        }
    }

    private void OpenDoor()
    {
        if (doorRenderer != null)
        {
            doorRenderer.enabled = false;
        }

        if (doorCollider != null)
        {
            doorCollider.enabled = false;
        }
    }

    private void CloseDoor()
    {
        if (doorRenderer != null)
        {
            doorRenderer.enabled = true;
        }

        if (doorCollider != null)
        {
            doorCollider.enabled = true;
        }
    }
}

