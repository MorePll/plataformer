using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderController : MonoBehaviour
{
    public ButtonController buttonController;
    public List<Renderer> ladderRenderers;
    public List<Collider> ladderColliders; // A�adimos la lista de Colliders
    public int ladderID;

    private void Start()
    {
        ladderRenderers = new List<Renderer>();
        ladderColliders = new List<Collider>();

        Renderer[] renderers = GetComponentsInChildren<Renderer>();
        Collider[] colliders = GetComponentsInChildren<Collider>();

        // Los agrego a las listas
        foreach (Renderer renderer in renderers)
        {
            ladderRenderers.Add(renderer);
        }

        foreach (Collider collider in colliders)
        {
            ladderColliders.Add(collider);
        }

        HideLadder();
    }

    private void Update()
    {
        if (buttonController.IsPressed() && buttonController.buttonID == ladderID)
        {
            ShowLadder();
        }
        else
        {
            HideLadder();
        }
    }

    private void HideLadder()
    {

        foreach (Renderer renderer in ladderRenderers)
        {
            renderer.enabled = false;
        }

        foreach (Collider collider in ladderColliders)
        {
            collider.enabled = false;
        }
    }

    private void ShowLadder()
    {

        foreach (Renderer renderer in ladderRenderers)
        {
            renderer.enabled = true;
        }

        foreach (Collider collider in ladderColliders)
        {
            collider.enabled = true;
        }
    }
}

