using UnityEngine;

public class Parallax : MonoBehaviour
{
    [SerializeField] private float plarallaxMultiplier;

    public Transform cameraTransform;
    private Vector3 previousCameraPosition;
    public float movumientoX;
    public float movumientoY;
    private float spriteWigth, startPosition;
    
    void Start()
    {
        cameraTransform = Camera.main.transform;
        previousCameraPosition = cameraTransform.position;
        spriteWigth = GetComponent<SpriteRenderer>().bounds.size.x;
        startPosition = transform.position.x;
    }

    void LateUpdate()
    {

        float camLeft = cameraTransform.position.x - cameraTransform.GetComponent<Camera>().orthographicSize * cameraTransform.GetComponent<Camera>().aspect;
        float camRight = cameraTransform.position.x + cameraTransform.GetComponent<Camera>().orthographicSize * cameraTransform.GetComponent<Camera>().aspect; 
        float moveAmount = cameraTransform.position.x * (1 - plarallaxMultiplier);
        // Calcula el cambio en la posici�n de la c�mara desde el fotograma anterior
        Vector3 deltaCameraPosition = cameraTransform.position - previousCameraPosition;

        // Actualiza la posici�n del paralaje en funci�n del cambio de la c�mara
        transform.position += new Vector3(deltaCameraPosition.x * movumientoX * -1f, deltaCameraPosition.y * movumientoY, 0);

        // Guarda la posici�n de la c�mara para el siguiente fotograma
        previousCameraPosition = cameraTransform.position;

        if(transform.position.x + spriteWigth / 2 < camLeft)
        {
            transform.Translate(new Vector3(spriteWigth, 0, 0));
            startPosition += spriteWigth;
        }
        else if(transform.position.x - spriteWigth / 2 > camRight)
        {
            transform.Translate(new Vector3(-spriteWigth, 0, 0));
            startPosition -= spriteWigth;
        }
    }
}
